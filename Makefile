install: 
		@echo Installing...
		@cp ./mullvad /usr/local/bin/
		@cp ./completion/mullvad /usr/share/bash-completion/completions/
		@su -l $$SUDO_USER -c 'mkdir -p $$HOME/.config/mullvad'	
		
		@sleep 1
		@echo "Installed! Update the servers with 'mullvad update servers' before trying to connect"

remove:
		@echo Removing files...
		@rm /usr/local/bin/mullvad
		@rm /usr/share/bash-completion/completions/mullvad
	
		@sleep 1
		@echo "Removed"
